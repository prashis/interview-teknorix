import { configureStore } from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";
import { all } from "redux-saga/effects";

// reducers & sagas
import { profileReducer, profileSaga } from "modules/profile";

const sagaMiddleware = createSagaMiddleware();

export default configureStore({
  reducer: {
    profile: profileReducer,
  },
  middleware: (def) => def().concat(sagaMiddleware),
});

sagaMiddleware.run(function* () {
  yield all([profileSaga()]);
});
