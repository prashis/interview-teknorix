import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import { Provider } from "react-redux";
import "antd/dist/antd.css";

import store from "store";

import Login from "pages/Login";
import Mailbox from "pages/Mailbox";

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        <Route path="/login" component={Login} />
        <Route path="/mailbox" component={Mailbox} />
      </Switch>
      <Redirect to="/login" />
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);
