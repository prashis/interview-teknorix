import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Form, Input, Button, Checkbox } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";

import { login } from "modules/profile";

export default function Login() {
  const dispatch = useDispatch();
  const profile = useSelector((state) => state.profile);
  const [username, setUsername] = useState("demouser@macrosoft.com");
  const [password, setPassword] = useState("Test_1234");

  const onSubmit = () => {
    dispatch(login({ username, password }));
  };

  return (
    <Form onSubmitCapture={onSubmit}>
      <Form.Item
        name="username"
        rules={[{ required: true, message: "Required username!" }]}
      >
        <Input
          placeholder="admin"
          prefix={<UserOutlined />}
          onChange={(e) => setUsername(e.target.value)}
          value={username}
        />
      </Form.Item>
      <Form.Item
        name="password"
        rules={[{ required: true, message: "Required password!" }]}
      >
        <Input.Password
          placeholder="888888"
          prefix={<LockOutlined />}
          onChange={(e) => setPassword(e.target.value)}
          value={password}
        />
      </Form.Item>
      <Form.Item name="remember" valuePropName="checked">
        <Checkbox>Remember me</Checkbox>
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit" block>
          Login
        </Button>
      </Form.Item>
    </Form>
  );
}
