import { createAction, createReducer } from "@reduxjs/toolkit";
import { all, call, put, takeEvery } from "redux-saga/effects";
import { userLogin, userProfile } from "utils/apis/profileApi";
import { notification } from "antd";

const initialState = {
  name: null,
  avatarUrl: null,
  email: null,
  authorized: false,
  loading: false,
};

export const login = createAction("profile/LOGIN");
const loginRequest = createAction("profile/LOGIN_REQUEST");
const loginSuccess = createAction("profile/LOGIN_SUCCESS");
const loginFailed = createAction("profile/LOGIN_FAILED");

export const loadProfile = createAction("profile/LOAD_PROFILE");
const loadProfileRequest = createAction("profile/LOAD_PROFILE_REQUEST");
const loadProfileSuccess = createAction("profile/LOAD_PROFILE_SUCCESS");
const loadProfileFailed = createAction("profile/LOAD_PROFILE_FAILED");

export const profileReducer = createReducer(initialState, {
  [loginRequest]: (state) => {
    state.loading = true;
  },
  [loginSuccess]: (state, action) => {
    state.authorized = true;
    state.loading = false;
  },
  [loginFailed]: (state) => {
    state.loading = false;
    state.authorized = false;
  },
  [loadProfileRequest]: (state) => {
    state.loading = true;
  },
  [loadProfileSuccess]: (state, action) => {
    state.email = action.payload.email;
    state.name = action.payload.name;
    state.avatarUrl = action.payload.avatarUrl;
  },
  [loadProfileFailed]: (state) => {
    state.loading = false;
    state.authorized = false;
  },
});

function* LOGIN({ payload }) {
  yield put(loginRequest());
  const response = yield call(userLogin, payload);
  if (response.token) {
    yield put(loginSuccess(response));
  }
  if (response.error) {
    notification.warning({
      message: response.error.message,
    });
    yield put(loginFailed());
  }
}

function* LOAD_PROFILE() {
  yield put(loadProfileRequest());
  const response = yield call(userProfile);
  if (response.email) {
    yield put(loadProfileSuccess(response));
  }
  if (response.error) {
    console.log({ error: response.error });
    notification.warning({
      message: response.error.message,
    });
    yield put(loadProfileFailed());
  }
}

export function* profileSaga() {
  yield all([
    takeEvery(login.type, LOGIN),
    takeEvery(loadProfile.type, LOAD_PROFILE),
    LOAD_PROFILE(),
  ]);
}
