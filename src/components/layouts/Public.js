import React, { Children } from "react";

export default function Public({ children }) {
  return <div>{children}</div>;
}
