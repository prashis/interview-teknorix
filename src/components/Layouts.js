import React from "react";

// before & after auth layouts
import PublicLayout from "components/layouts/Public";
import AuthLayout from "components/layouts/Auth";

const Layouts = {
  public: PublicLayout,
  auth: AuthLayout,
};

export default function Layout() {
  // const BootstrapLayout = () => {
  // }
  return <div></div>;
}
