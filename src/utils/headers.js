export function authHeader() {
  const profile = JSON.parse(localStorage.getItem("user") || `{"token": null}`);
  return {
    Authorization: "Bearer " + profile.token || null,
  };
}
