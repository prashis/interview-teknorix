import configs from "configs";
import axios from "axios";
import { authHeader } from "utils/headers";

const { BASE_API_PUBLIC, BASE_API_AUTH } = configs;

export const userLogin = async ({ username, password }) => {
  try {
    const url = `${BASE_API_AUTH}/login`;
    const body = { username, password };
    const response = await axios.post(url, body);
    if (!response.data.token) throw new Error("Failed login!");
    localStorage.setItem("user", JSON.stringify(response.data));
    return response.data;
  } catch (error) {
    return { error };
  }
};

export const userProfile = async () => {
  try {
    const url = `${BASE_API_AUTH}/profile`;
    const response = await axios.get(url, { headers: authHeader() });
    return response.data;
  } catch (error) {
    return { error };
  }
};
